#ifndef GENFOLDERSFOOS_H
#define GENFOLDERSFOOS_H

// defined in genfoldersfoos.c
extern bool debug;
extern bool debug2;

// adjust for longest line and cell in csv
#define MAXLINE 2048
#define MAXCELL 512

// adjust for correct file delimiter in csv file (must specify whitespace)
#define DELIM ",";

// errors
#define USAGE_ERR 1
#define READ_ERR 2
#define COL_ERR 3


// Returns the column number of the specified column in a csv file.
// Returns -1 if column not in file.
int findTargetColumnNumber(FILE* stream, char *targetColumn);

// Decides whether or not a string contains a '\n' character
bool containsNewLine(char *string);

// gets the last token from a string delimited by DELIM, where last token is
// terminated by '\n'
char *getLastToken(char *string, const char *delim);

char *terminatedZeroString(int size);

// Prints a help message to explain usage
void printHelp(void);

// creates a directory for each cell below the header in column targetColumn
// of csv table pointed to by stream
void createDirs(FILE *in, FILE *out, int targetColumn);

// reads the next cell from the csv, removes trailing whitespace and makes
// a dir names after it if it does not already exist.
// Prints a message to stdout if dir already exists.
int readCellMkDir(FILE *in, FILE *out);

FILE *removePreceedingRow(FILE *out);

// Removes trailing whitespace ("\t\r\\n ") from a string
void removeTrailingWhiteSpace(char *string);

// Returns the number of rows in a given csv file, where rows are terminated
// with a new line character
int rowsInCSVFile(FILE *stream);

// moves a stream pointer to after the next '\n', or EOF.
void moveStreamsToEndOfLine(FILE *in, FILE *out);

// moves a stream pointer to after the next instance of given character, or
// or '\n' or EOF.
// returns 0 if found an instance, or 1 if EOF.
int moveStreamsToNextInstanceOfDelimOrNewLine(FILE *in, FILE *out, char delim);

#endif

# Makefile
# Cutover

# flags
CFLAGS = -O0 -g -Wall -Werror

# main target
all: genfolders.c genfoldersfoos.c genfoldersfoos.h
	clang -std=c99 $(CFLAGS) genfolders.c genfoldersfoos.c -o genfolders

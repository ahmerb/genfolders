#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "genfoldersfoos.h"


char delim[2] = DELIM;


// Returns the column number of the specified column in a csv file.
// Returns -1 if column not in file.
int findTargetColumnNumber(FILE* stream, char *targetColumn)
{
    if (debug) printf("targetColumn = %s\n", targetColumn);

    // get initial stream position for returning to upon return
    long initialPos = ftell(stream);

    // store the column number of target column
    int columnNumber = 0;

    // reset stream to start
    rewind(stream);

    // read the header (first) line
    char *line = terminatedZeroString(MAXLINE);
    if (fgets(line, MAXLINE, stream) == NULL) fprintf(stderr, "read error\n");

    if (debug) printf("strlen header line = %lu\n", strlen(line));

    // read the first cell
    char *cell = strtok(line, delim);
    if (debug) printf("cell = %s\n", cell);
    if (debug) printf("columnNumber = %d\n", columnNumber);

    // iterate by looking for substring followed by commma, or stopping at EOF
    bool force_break_next_itr = false;
    while (!feof(stream))
    {
        // if we looked at all columns, break and return err value
        if (force_break_next_itr)
        {
            columnNumber = -1;
            break;
        }

        // if end of line, split at new line to retrieve last column, and then
        // set variable to break on next iteration
        if (containsNewLine(cell) == 0)
        {
            if (debug) printf("containsNewLine(cell) == 0\n");
            force_break_next_itr = true;
            // remove terminating '\n'
            cell[strcspn(cell, "\n")] = '\0';
            if (debug) printf("cell = %s\n", cell);
            //cell = getLastToken(line, delim);
            //if (debug) printf("cell = %s\n", cell);
        }

        // if it is target column, break from loop, else incr columnNumber
        if (strcmp(cell, targetColumn) == 0)
        {
            if (debug) printf("strcmp(cell, targetColumn) == 0\n");
            break;
        }
        else
        {
            columnNumber++;
        }
        if (debug) printf("columnNumber = %d\n", columnNumber);

        // read substring until the next comma, store in cell
        cell = strtok(NULL, delim);
        if (debug) printf("cell = %s\n", cell);

    }

    // move stream back to initial pos and return
    fseek(stream, initialPos, SEEK_SET);
    if (debug) printf("columnNumber = %d\n", columnNumber);
    free(line);
    return columnNumber;
}


// Decides whether or not a string contains a '\n' character
bool containsNewLine(char *string)
{
    char *newLine = strchr(string, '\n');
    return newLine == '\0' ? true : false;
}


// gets the last token from a string delimited by delim, where last token is
char *getLastToken(char *string, const char *delim)
{
    // set last to start of last occurrence of delim string
    char *last = strrchr(string, delim[0]);

    // skip over rest of delimiting string also
    int delim_len = strlen(delim);
    for (int i = 0; i < delim_len; i++)
    {
        last++;
    }

    // remove terminating '\n'
    last[strcspn(last, "\n")] = '\0';

    if (debug) printf("getLastToken_last = %s\n", last);
    return last;
}


// returns a pointer to null-term'd zero string on the heap of size given
// freeing is left to the user.
char *terminatedZeroString(int size)
{
    char *string = malloc(size);
    for (int i = 0; i < size; i++)
        string[i] = '0';
    string[size-1] = '\0';
    return string;
}


// Prints a help message to explain usage
void printHelp(void)
{
    printf("Usage:\n");
    printf("./genfolders <table.csv> <target_column>\n");
    printf("./genfolders help\n");
    printf("To enable/disable debug messages, set variable debug at top of" \
            "genfolders.c\n");
    printf("To set the delimiting character for the csv file, set constant" \
            "DELIM at the top of genfolderfoos.h\n");
}


// creates a directory for each cell below the header in column targetColumn
// of csv table pointed to by stream
void createDirs(FILE *in, FILE *out, int targetColumn)
{
    if (debug) printf("\n\ncreateDirs\n");

    // store the current stream pos' for returning to later
    long initialPos_in = ftell(in);
    long initialPos_out = ftell(out);

    // move streams to start of csv file
    rewind(in);
    rewind(out);

    // move streams to start of second line
    if (debug) printf("calling moveStreamToEndOfLine\n");
    moveStreamsToEndOfLine(in, out);

    if (debug) printf("rowsinCSVFile = %d\n", rowsInCSVFile(in));
    if (debug) { if (feof(in)) printf("AT END OF FILE AFTER FIRST MOVE END OF LINE CALL\n"); }

    // make a directory for each cell in target column
    for (int i = 0, n = rowsInCSVFile(in); i < n; i++)
    {
        // move stream pointer to the next column
        for (int j = 0; j < targetColumn; j++)
        {
            //advance stream pointer to end of line
            if (debug) printf("calling moveStreamToNextInstanceOf with \'%c\'\n", delim[0]);
            moveStreamsToNextInstanceOfDelimOrNewLine(in, out, delim[0]);
        }

        // read the cell and make a directory named after it
        // write the cell only if it doesn't already exist
        int duplicate = readCellMkDir(in, out);

        // move to end of line
        if (debug) printf("calling moveStreamToEndOfLine\n");
        moveStreamsToEndOfLine(in, out);

        // TODO if duplicate, remove preceeding row from out
        if (duplicate == 1)
            out = removePreceedingRow(out);
    }

    // move stream pointer back and return
    fseek(in, initialPos_in, SEEK_SET);
    fseek(out, initialPos_out, SEEK_SET);
}


// reads the next cell from the csv, removes trailing whitespace and makes
// a dir names after it if it does not already exist.
// Prints a message to stdout if dir already exists.
// Returns 0 if there is no dir already name.
// Returns 1 if dir already exists.
int readCellMkDir(FILE *in, FILE *out)
{
    // return value to tell caller whether dir already exists
    int duplicate = 1;

    // malloc space for the next cell
    char *cell = malloc(MAXCELL);

    if (debug) printf("createDirs_cell=%s\n", cell);
    if (debug) { if (feof(in)) printf("feof(in)\n"); }

    // read the next cell
    fscanf(in, "%[^,\n]", cell);
    removeTrailingWhiteSpace(cell);
    fprintf(out, "%s%c", cell, fgetc(in));

    // create the dir only if doesn't exist
    struct stat st = {0};
    if (stat(cell, &st) != -1)
    {
        printf("dir already exits: %s\n", cell);
        duplicate = 1;
    }
    mkdir(cell, 0777);  // mode: do not mkdir if already exists

    // all done
    //***TODO MEMORY LEAK*** no free(cell);
    return duplicate;
}


// removes the preceeding row from the outfile csv stream. Stream pointer
// must be at end of row.
FILE *removePreceedingRow(FILE *out)
{
    if (debug2) printf("called removePreceedingRow\n");

    // create a new file that will not have preceeding row
    FILE *new = fopen("new.csv", "w+");

    // move stream back 2 chars to before the newline
    fseek(out, -2, SEEK_CUR);

    // move stream back until it encounters another newline or start of file
    // (ie move to start of row)
    while (ftell(out) != 0 && fgetc(out) != '\n')
    {
        fseek(out, -2, SEEK_CUR);
        //if (debug2) printf("called fseek\n");
        //if (debug2) printf("ftell(out)=%ld\n", ftell(out));
    }

    // store position of new end of file
    int startOfRow = ftell(out);

    // write the csv without the row to new outfile
    rewind(out);
    for (int i = 0; i < startOfRow; i++)
    {
        char c = fgetc(out);
        fputc(c, new);
    }

    // close and delete out, replace with new outfile
    fclose(out);
    remove("new_table.csv");
    fclose(new);
    rename("new.csv", "new_table.csv");
    new = fopen("new_table.csv", "w+");

    // TODO write the rest of old outfile to new outfile

    return new;
}


// Removes trailing whitespace ("\t\r\\n ") from a string
void removeTrailingWhiteSpace(char *string)
{
    // find last non-whitespace character
    char *end = string + strlen(string) - 1;
    while(end > string && isspace(*end))
        end--;

    // Write new null terminator
    *(end+1) = '\0';
}


// Returns the number of rows in a given csv file, where rows are terminated
// with a new line character
int rowsInCSVFile(FILE *stream)
{
    int initial_pos = ftell(stream);
    rewind(stream);
    int row_count = 0;
    while (!feof(stream))
    {
        moveStreamsToEndOfLine(stream, NULL);
        row_count++;
    }
    fseek(stream, initial_pos, SEEK_SET);
    return row_count;
}


// moves a stream pointer to after the next '\n', or EOF.
void moveStreamsToEndOfLine(FILE *in, FILE *out)
{

    char tmp[MAXLINE];
    fgets(tmp, MAXLINE, in);
    if (out != NULL) fprintf(out, "%s", tmp);
    //if (debug) printf("ftell(stream) = %ld\n", ftell(stream));
}


// moves a stream pointer to after the next instance of given character,
// or '\n' or EOF.
// returns 0 if found an instance, or 1 if EOF.
int moveStreamsToNextInstanceOfDelimOrNewLine(FILE *in, FILE *out, char delim)
{
    if (debug) { if (feof(in)) printf("**feof(in)**\n"); }

    char c = fgetc(in);
    if (out != NULL) fputc(c, out);
    //if (debug) printf("called fgetc(stream)\n");
    while (c != delim && c != '\n' && c != EOF)
    {
        c = fgetc(in);
        //if (debug) printf("called fgetc(stream)\n");
        if (out != NULL) fputc(c, out);
    }
    if (c == EOF) return 1;
    else return 0;
}

/**
* Generates a directory named after each cell in a given column in a given
* csv file.
*
* Cutover
* Ahmer Butt
*/

// libs
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "genfoldersfoos.h"

bool debug = false;
bool debug2 = true;

// read in a csv and create diirectory named after each cell in the given
// column
int main(int argc, char *argv[])
{
    if (argc == 2 && strcmp(argv[1], "help") == 0)
    {
        printHelp();
        exit(0);
    }


    // check argc == 3 (csv file and column header)
    if (argc != 3)
    {
        fprintf(stderr, "error: incorrect usage\n");
        printHelp();
        exit(USAGE_ERR);
    }

    // open csv file, and new file to write too
    FILE* in_table = fopen(argv[1], "r");
    FILE* out_table = fopen("new_table.csv", "w+");
    if (in_table == NULL)
    {
        fprintf(stderr, "error2: csv file not found\n");
        exit(READ_ERR);
    }

    // find the correct column number
    int targetColumn = findTargetColumnNumber(in_table, argv[2]);
    if (debug) printf("main_targetColumn = %d\n", targetColumn);

    // exit if target column not found
    if (targetColumn == -1)
    {
        fprintf(stderr, "error3: target column not found in csv file\n");
        exit(COL_ERR);
    }

    // create folder for each cell in specified column
    createDirs(in_table, out_table, targetColumn);

    // all done
    fclose(in_table);
    fclose(out_table);
    return 0;
}
